# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class GenerateFakturPajak(models.TransientModel):
    _name = 'generate.faktur.pajak'

    def get_faktur_type(self):
        return self._context.get('type', False)

    nomor_faktur_awal = fields.Char('Nomor Faktur Awal', size=20)
    nomor_faktur_akhir = fields.Char('Nomor Faktur Akhir', size=20)
    nomor_perusahaan = fields.Char('Nomor Perusahaan', size=3, required=True)
    nomor_awal = fields.Char('Nomor Faktur Awal', size=8, required=True)
    nomor_akhir = fields.Char('Nomor Faktur Akhir', size=8, required=True)
    tahun = fields.Char('Tahun Penerbit', size=2,  required=True)
    type = fields.Selection([
            ('in','Faktur Pajak Masukan'),
            ('out','Faktur Pajak Keluaran')
        ],'Type', required=True, default='in')

    def generate_faktur(self):
        awal = int(self.nomor_awal)
        akhir = int(self.nomor_akhir)
        if akhir <= awal:
            message = _('Wrong input, Nomor Akhir must be greater than Nomor Awal')
            raise ValidationError(message)

        else:
            while (awal <= akhir):
                value = {
                    'nomor_perusahaan': self.nomor_perusahaan,
                    'tahun_penerbit': self.tahun,
                    'nomor_urut': '%08d' % awal,
                    'status': '0',
                    'type': self.type,
                }
                self.env['nomor.faktur.pajak'].create(value)
                awal += 1

            return {'type': 'ir.actions.act_window_close'}
