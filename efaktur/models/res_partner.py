# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class Partner(models.Model):
    _inherit = "res.partner"
    
    type = fields.Selection([('default', 'Default'), 
                              ('invoice', 'Invoice'),
                               ('delivery', 'Shipping'), 
                               ('contact', 'Contact'),
                               ('npwp', 'NPWP'),
                               ('other', 'Other')], 'Address Type',
            help="Used to select automatically the right address according to the context in sales and purchases documents.")
    #nama_npwp = fields.Char('Nama NPWP')
    npwp = fields.Char('NPWP', size=20, default='00.000.000.0-000.000')
    kawasan = fields.Selection([('yes','YES'),
                                ('no','NO')], 'Kawasan', 
                                help='',)
    kode_transaksi = fields.Selection([('010','010 Normal'),
                                        ('020','020 Bendaharawan (Tdk Terlampir)'),
                                        ('030','030 Bendaharawan (Terlampir)'),
                                        ('080','080 Tanpa PPN')], 
                                        'No. Seri Faktur',
                                        default='010')
    rt = fields.Char('RT', size=3)
    rw = fields.Char('RW', size=3)
    kecamatan_id = fields.Many2one('res.kecamatan',"Kecamatan")
    kabupaten_id = fields.Many2one('res.kabupaten',"Kabupaten")
    kelurahan_id = fields.Many2one('res.kelurahan',"Kelurahan")

    @api.onchange('npwp')
    def _onchange_npwp(self):
        if len(self.npwp) == 15:
            formatted_npwp = npwp[:2]+'.'+npwp[2:5]+'.'+npwp[5:8]+'.'+npwp[8:9]+'-'+npwp[9:12]+'.'+npwp[12:15]
            self.npwp = formatted_npwp
        elif not len(self.npwp) == 20:
            warning = {
                'title': _('Warning '),
                'message': _('Wrong Format %s, NPWP must in 15 digit format.' % (self.npwp)),
            }
            return {'warning': warning, 'value' : {'npwp' : False}}
