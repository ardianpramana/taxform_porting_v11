# -*- coding: utf-8 -*-

from odoo import models, fields, api
import openerp.addons.decimal_precision as dp


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    nomor_faktur_id = fields.Many2one('nomor.faktur.pajak', string='Nomor Faktur Pajak', required=False)
    kode_transaksi = fields.Selection([
    						('010','010.'),
							('020','020.'),
							('030','030.'),
							('080','080.')
						], string='Kode Faktur', compute='_nomor_faktur_partner', 
        							store=True, readonly=True)
    nomor_faktur_partner = fields.Char(string='Nomor Faktur', digits=dp.get_precision('Account'),
    							store=True, readonly=True, compute='_nomor_faktur_partner')
    npwp_no = fields.Char(string='NPWP', compute='_nomor_faktur_partner', store=True, readonly=True)
    npwp_efaktur = fields.Char(string='NPWP for eFaktur', compute='_nomor_faktur_partner', store=False, readonly=True)
    vat_supplier = fields.Char(string='Faktur Pajak No', size=63, readonly=True, 
						states={'draft': [('readonly', False)]}, index=True, 
						help="Nomor Bukti Potong", copy=False)

    @api.multi
    @api.depends('partner_id.kode_transaksi', 'nomor_faktur_id', 'nomor_faktur_id.name')
    def _nomor_faktur_partner(self):
        for invoice in self:
            invoice.nomor_faktur_partner = "%s.%s" % (invoice.partner_id.kode_transaksi,invoice.nomor_faktur_id and invoice.nomor_faktur_id.name)
            invoice.kode_transaksi = invoice.partner_id.kode_transaksi
            invoice.npwp_no = invoice.partner_id.npwp
            if invoice.npwp_no:
                npwp = invoice.npwp_no
                npwp = npwp.replace('.','')
                npwp = npwp.replace('-','')
                invoice.npwp_efaktur = npwp
