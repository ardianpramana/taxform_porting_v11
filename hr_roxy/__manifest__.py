# -*- coding: utf-8 -*-
# Copyright 2018 Arkana Solusi Digital
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Roxy Human Resource Management",
    "version": "11.0.1.0.0",
    "category": "hr",
    "website": "https://www.arkana.co.id/",
    "author": "Arkana Solusi Digital, "
              "Ardian",
    "license": "AGPL-3",
    "application": True,
    "installable": True,
    "depends": [
        "hr",
        "hr_contract",
        "l10n_id_taxform",
    ],  
    "data": [
        "views/hr_employee_views.xml",
        "views/hr_contract_views.xml",
    ],
}
