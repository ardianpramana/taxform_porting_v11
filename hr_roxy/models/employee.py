# -*- coding: utf-8 -*-
# Copyright 2018 Arkana Solusi Digital
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from openerp import models, fields, api


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    npwp = fields.Char('NPWP', size=20, default='00.000.000.0-000.000')
