# -*- coding: utf-8 -*-
# Copyright 2018 Arkana Solusi Digital
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from openerp import models, fields, api


class HrContract(models.Model):
    _inherit = 'hr.contract'


    functional_alw = fields.Float('Tunjangan jabatan')
    expertise_alw = fields.Float('Tunjangan keahlian')
    diligence_alw = fields.Float('Tunjangan kerajinan')
    meal_alw = fields.Float('Tunjangan makan')
    transport_alw = fields.Float('Tunjangan transport')
    phone_bill_alw = fields.Float('Tunjangan pulsa')
    rent_bike_alw = fields.Float('Tunjangan sewa motor')
    area_olm_alw = fields.Float('Tunjangan Area Manager')
